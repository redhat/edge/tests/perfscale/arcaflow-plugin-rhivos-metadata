#!/usr/bin/env python3
import unittest
import rhivos_metadata_plugin
import rhivos_metadata_schema
from arcaflow_plugin_sdk import plugin


class MetadataTest(unittest.TestCase):
    @staticmethod
    def test_serialization():
        plugin.test_object_serialization(rhivos_metadata_plugin.InputParams())

        plugin.test_object_serialization(
            rhivos_metadata_plugin.SuccessOutput(
                kernel="6.8.7-402.asahi.fc39.aarch64+16k",
                osrelease="Fedora Linux Asahi Remix 39 (Workstation Edition)",
                os_name="Fedora Linux Asahi Remix",
                os_id="fedora-asahi-remix",
                os_ver="39",
                architecture="aarch64",
                model="Blizzard-M2-Pro",
                numcores=12,
                bogomips=48.0,
                build="Unknown",
                release="Unknown",
                image_name="Unknown",
                image_target="Unknown",
                image_mode="Unknown",
                image_timestamp="Unknown",
                cmdline="BOOT_IMAGE=(hd0,gpt5)/vmlinuz-6.8.7-402.asahi.fc39.aarch64+16k root=UUID=6b7fba8b-c7ab-48fe-b5b2-a3ce1dc9476f "
                        "ro rootflags=subvol=root rhgb quiet rootflags=subvol=root",
                installed_rpms=[
                    "gpg-pubkey-18b8e74c-62f2920f",
                    "gpg-pubkey-3aa33141-6395721b",
                ]
            )
        )

        plugin.test_object_serialization(
            rhivos_metadata_plugin.ErrorOutput(error="This is an error")
        )

    def test_functional(self):
        input = rhivos_metadata_plugin.InputParams()

        output_id, output_data = rhivos_metadata_plugin.collect_metadata(
            params=input, run_id="plugin_ci"
        )

        self.assertEqual("success", output_id)
        self.assertIsInstance(output_data, rhivos_metadata_schema.SuccessOutput)
        self.assertGreaterEqual(len(output_data.architecture), 1)
        self.assertIsInstance(output_data.kernel, str)
        self.assertIsInstance(output_data.numcores, int)
        self.assertIsInstance(output_data.bogomips, float)


if __name__ == "__main__":
    unittest.main()
