#!/usr/bin/env python3

import subprocess
import sys
import typing
import re
import os
import json

from arcaflow_plugin_sdk import plugin
from rhivos_metadata_schema import (
    InputParams,
    SuccessOutput,
    ErrorOutput,
    success_output_schema,
)


def run_command(cmd: str, timeout: int = 20) -> typing.Tuple[int, str, str]:
    cmd_list = cmd.split()

    try:
        cmd_proc = subprocess.Popen(
            cmd_list,
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout_result, stderr_result = cmd_proc.communicate(timeout=timeout)

    except subprocess.CalledProcessError:
        raise

    except subprocess.TimeoutExpired:
        raise

    exit_status = cmd_proc.returncode

    return (exit_status, stdout_result, stderr_result)


def verify_trim(value):
    if not value:
        ret_val = None
    else:
        ret_val = str(value.strip())

    return ret_val


def parse_lscpu(cmd_out, the_dict):
    the_dict = {
        "architecture": "Unknown",
        "model": "Unknown",
        "numcores": 0,
        "bogomips": 0.0,
    }

    for line in cmd_out.split("\n"):
        if "Architecture:" in line:
            the_dict["architecture"] = verify_trim(re.search("Architecture:(.*)", cmd_out).group(1))
        elif "Model name:" in line:
            the_dict["model"] = verify_trim(re.search("Model name.*:(.*)", cmd_out).group(1))
        elif "CPU(s):" in line:
            the_dict["numcores"] = int(verify_trim(re.search("CPU\\(s\\):(.*)", cmd_out).group(1)))
        elif "BogoMIPS:" in line:
            the_dict["bogomips"] = float(verify_trim(re.search("BogoMIPS:(.*)", cmd_out).group(1)))

    return the_dict


def parse_osrelease(cmd_out, the_dict):
    the_dict = {
        "osrelease": "Unknown",
        "os_name": "Unknown",
        "os_id": "Unknown",
        "os_ver": "Unknown",
    }
    for line in cmd_out.split("\n"):
        if "PRETTY_NAME=" in line:
            raw_str = re.search("PRETTY_NAME=(.*)", cmd_out).group(1)
            osrelease = verify_trim(raw_str.replace('"', ""))
            if osrelease:
                the_dict["osrelease"] = osrelease
        elif "NAME=" in line:
            raw_str = re.search("NAME=(.*)", cmd_out).group(1)
            os_name = verify_trim(raw_str.replace('"', ""))
            if os_name:
                the_dict["os_name"] = os_name
        elif "VERSION_ID=" in line:
            raw_str = re.search("VERSION_ID=(.*)", cmd_out).group(1)
            os_ver = verify_trim(raw_str.replace('"', ""))
            if os_ver:
                the_dict["os_ver"] = os_ver
        elif "ID=" in line:
            raw_str = re.search("ID=(.*)", cmd_out).group(1)
            os_id = verify_trim(raw_str.replace('"', ""))
            if os_id:
                the_dict["os_id"] = os_id

    return the_dict


def parse_build(cmd_out, the_dict):
    the_dict = {
        "build": "Unknown",
        "release": "Unknown",
        "image_name": "Unknown",
        "image_target": "Unknown",
        "image_mode": "Unknown",
        "image_timestamp": "Unknown",
    }
    for line in cmd_out.split("\n"):
        if "UUID=" in line:
            raw_str = re.search("UUID=(.*)", cmd_out).group(1)
            build = verify_trim(raw_str.replace('"', ""))
            if build:
                the_dict["build"] = build
        elif "RELEASE=" in line:
            raw_str = re.search("RELEASE=(.*)", cmd_out).group(1)
            release = verify_trim(raw_str.replace('"', ""))
            if release:
                the_dict["release"] = release
        elif "IMAGE_NAME=" in line:
            raw_str = re.search("IMAGE_NAME=(.*)", cmd_out).group(1)
            image_name = verify_trim(raw_str.replace('"', ""))
            if image_name:
                the_dict["image_name"] = image_name
        elif "IMAGE_TARGET=" in line:
            raw_str = re.search("IMAGE_TARGET=(.*)", cmd_out).group(1)
            image_target = verify_trim(raw_str.replace('"', ""))
            if image_target:
                the_dict["image_target"] = image_target
        elif "IMAGE_MODE=" in line:
            raw_str = re.search("IMAGE_MODE=(.*)", cmd_out).group(1)
            image_mode = verify_trim(raw_str.replace('"', ""))
            if image_mode:
                the_dict["image_mode"] = image_mode
        elif "TIMESTAMP=" in line:
            raw_str = re.search("TIMESTAMP=(.*)", cmd_out).group(1)
            image_timestamp = verify_trim(raw_str.replace('"', ""))
            if image_timestamp:
                the_dict["image_timestamp"] = image_timestamp

    return the_dict


@plugin.step(
    id="collect-metadata",
    name="Collect RHIVOS Metadata",
    description="Collects important system metadata from a RHIVOS or AutoSD system.",
    outputs={"success": SuccessOutput, "error": ErrorOutput},
)
def collect_metadata(
    params: InputParams,
) -> typing.Tuple[str, typing.Union[SuccessOutput, ErrorOutput]]:
    meta_dict = {}

    cmd_list = [
        ("kernel", "uname -r"),
        ("osrelease", f"cat {params.etc_path}/os-release"),
        ("various", "lscpu"),
        ("build", f"cat {params.etc_path}/build-info"),
        ("cmdline", "cat /proc/cmdline"),
        ("installed_rpms", "rpm -qa"),
    ]

    for key, cmd_str in cmd_list:
        try:
            _, stdout, _ = run_command(cmd_str)

            if key == "osrelease":
                meta_dict.update(parse_osrelease(stdout, meta_dict))
            elif key == "various":
                meta_dict.update(parse_lscpu(stdout, meta_dict))
            elif key == "build":
                meta_dict.update(parse_build(stdout, meta_dict))
            elif key == "installed_rpms":
                meta_dict[key] = stdout.splitlines()
            else:
                meta_dict[key] = str(verify_trim(stdout))

        except subprocess.CalledProcessError as e:
            return "error", ErrorOutput(f"Command Failed: {e}")

        except subprocess.TimeoutExpired as e:
            return "error", ErrorOutput(f"Connection Timeout Expired: {e}")

    # Initialize firmware keys
    meta_dict["firmware"] = "Unknown"
    meta_dict["qc_firmware_ver_info"] = {}
    for i in "Image_Build_IDs", "Metabuild_Info", "Version":
        meta_dict["qc_firmware_ver_info"][i] = "Unknown"

    # Conditionally collect firmware information
    if "image_target" in meta_dict.keys() and (
        ("ridesx4" in meta_dict["image_target"]) or
        ("CCUES" in meta_dict["kernel"])
    ):
        print("**Target is RideSX4; Collecting Qualcomm firmware info...")

        if os.path.isfile(params.firmware_file_path):
            print("**Collecting firmware info...")
            _, stdout, _ = run_command(f"cat {params.firmware_file_path}")
            meta_dict["qc_firmware_ver_info"] = json.loads(stdout)
            meta_dict["firmware"] = meta_dict["qc_firmware_ver_info"]["Metabuild_Info"]["Meta_Build_ID"]

    return "success", success_output_schema.unserialize(meta_dict)


if __name__ == "__main__":
    sys.exit(
        plugin.run(
            plugin.build_schema(
                collect_metadata,
            )
        )
    )
