#!/usr/bin/env python3

import typing
from dataclasses import dataclass
from arcaflow_plugin_sdk import schema, plugin


@dataclass
class InputParams:
    etc_path: typing.Annotated[
        typing.Optional[str],
        schema.name("etc directory path"),
        schema.description(
            "Path within the container to look for etc directory contents. "
            "Useful when running in a container the host /etc is mounted "
            "somewhere non-standard."
        )
    ] = "/etc"
    firmware_file_path: typing.Annotated[
        typing.Optional[str],
        schema.name("qc firmware mount point"),
        schema.description(
            "Mount point and path within the container to look for the "
            "Qualcomm firmware file system."
        )
    ] = "/qc-firmware/verinfo/ver_info.txt"


@dataclass
class QCFirmwareInfo:
    image_build_ids: typing.Annotated[
        typing.Any,
        schema.id("Image_Build_IDs"),
        schema.name("qc image build ids"),
        schema.description("Qualcomm firmware image build IDs")
    ]
    metabuild_info: typing.Annotated[
        typing.Any,
        schema.id("Metabuild_Info"),
        schema.name("qc meta build info"),
        schema.description("Qualcomm firmware meta build info")
    ]
    version: typing.Annotated[
        str,
        schema.id("Version"),
        schema.name("qc version"),
        schema.description("Qualcomm firmware format version")
    ]


@dataclass
class SuccessOutput:
    kernel: typing.Annotated[
        str,
        schema.name("kernel version"),
        schema.description("System kernel version"),
    ]
    osrelease: typing.Annotated[
        str,
        schema.name("OS release"),
        schema.description("System OS release"),
    ]
    os_name: typing.Annotated[
        str,
        schema.name("OS name"),
        schema.description("System OS name"),
    ]
    os_id: typing.Annotated[
        str,
        schema.name("OS ID"),
        schema.description("System OS ID"),
    ]
    os_ver: typing.Annotated[
        str,
        schema.name("OS version"),
        schema.description("System OS version"),
    ]
    architecture: typing.Annotated[
        str,
        schema.name("system architecture"),
        schema.description("System architecture"),
    ]
    model: typing.Annotated[
        str,
        schema.name("system model"),
        schema.description("System model"),
    ]
    numcores: typing.Annotated[
        int,
        schema.name("number of cores"),
        schema.description("System number of CPU cores"),
    ]
    bogomips: typing.Annotated[
        float,
        schema.name("bogomips"),
        schema.description("System bogomips"),
    ]
    build: typing.Annotated[
        str,
        schema.name("RHIVOS build"),
        schema.description("RHIVOS build"),
    ]
    release: typing.Annotated[
        str,
        schema.name("RHIVOS release"),
        schema.description("RHIVOS release"),
    ]
    image_name: typing.Annotated[
        str,
        schema.name("RHIVOS name"),
        schema.description("RHIVOS image name"),
    ]
    image_target: typing.Annotated[
        str,
        schema.name("RHIVOS target"),
        schema.description("RHIVOS image target"),
    ]
    image_mode: typing.Annotated[
        str,
        schema.name("RHIVOS mode"),
        schema.description("RHIVOS build mode (image or package)"),
    ]
    image_timestamp: typing.Annotated[
        str,
        schema.name("RHIVOS timestamp"),
        schema.description("RHIVOS image timestamp"),
    ]
    cmdline: typing.Annotated[
        str,
        schema.name("OS command line"),
        schema.description("System OS command line"),
    ]
    installed_rpms: typing.Annotated[
        typing.List[str],
        schema.name("installed rpms"),
        schema.description("System installed RPMs"),
    ]
    firmware: typing.Annotated[
        typing.Optional[str],
        schema.name("system firmware"),
        schema.description("System firmware"),
    ] = None
    qc_firmware_ver_info: typing.Annotated[
        typing.Optional[QCFirmwareInfo],
        schema.name("qualcomm firmware info"),
        schema.description("Qualcomm firmware version info"),
    ] = None

success_output_schema = plugin.build_object_schema(SuccessOutput)


@dataclass
class ErrorOutput:
    error: str
